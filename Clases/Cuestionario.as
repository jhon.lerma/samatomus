﻿package Clases{
	public class Cuestionario {
		import Clases.Pregunta;
		import Clases.ErrorSonido;
		import Clases.PopUpSonido;
		import flash.filters.*;
		import flash.events.*;
		import flash.display.*;
		import flash.net.*;
		import flash.xml.*;
		import flash.text.*;
		import flash.filters.GlowFilter;
		var filtro:GlowFilter=new GlowFilter();
		var Padre:Object=new Object();
		var Tamaño:int;
		var resSel:String;
		var cargador:URLLoader=new URLLoader();
		var listaXml:Array=new Array();
		var Bloque:int;
		var posY:int;
		var Respuesta:String;
		public function Cuestionario(archivo:String,bloque:int,tamaño:int,padre) {
			Padre=padre;
			Bloque=bloque;
			Tamaño=tamaño;
			Padre.cayuda.bListo.addEventListener(MouseEvent.CLICK,compara);
			cargador.dataFormat=URLLoaderDataFormat.TEXT;
			cargador.load(new URLRequest("XML/"+archivo));
			cargador.addEventListener(Event.COMPLETE,cargado);
			filtro.blurX=20;
			filtro.blurY=20;
			filtro.alpha=1;
			filtro.color=0xff0000;
		}
		function cargado(e:Event) {
			var Xml:XML=new XML(e.target.data);
			for each (var nodXml:XML in Xml.pregunta) {
				if (nodXml.@indice==Bloque) {
					var preg:Pregunta=new Pregunta(nodXml.@pregunta + ". Seleccione la respuesta correcta realizando click sobre el respectivo item. ",Tamaño,Padre);
					Respuesta=nodXml.respuesta;
					for each (var childNodXml:XML in nodXml.item) {
						var preItem:Sprite=new Sprite  ;
						var texto:TextField=new TextField();
						var formato:TextFormat=new TextFormat();
						formato.font="Opal";
						formato.align=TextFormatAlign.CENTER
						formato.size=25;						
						texto.autoSize="left";
						texto.embedFonts=true;
						texto.text=childNodXml;
						texto.x=5;
						texto.y=5;
						texto.multiline=true;
						texto.wordWrap=true;
						texto.width=600
						texto.selectable=false;
						texto.mouseEnabled=false;
						texto.setTextFormat(formato);
						preItem.addChild(texto);
						//se dibuja el cuadro
						preItem.name=childNodXml;
						//trace(target.name)
						preItem.graphics.lineStyle(4,0x000000);
						preItem.graphics.beginFill(0x9988ee,0.9);
						preItem.graphics.drawRoundRect(0,0,texto.width+10,texto.height+10,10,10);
						preItem.addEventListener(MouseEvent.CLICK,seleccion);
						Padre.contenedor.addChild(preItem);
						preItem.y=posY;
						posY=posY+preItem.height+20;
					}
				}
			}
		}
		function seleccion(e:Event) {
			resSel=e.target.name;
			for (var num:int; num<Padre.contenedor.numChildren; num++) {
				Padre.contenedor.getChildAt(num).filters=null;
			}
			e.target.filters=[filtro];
		}
		function compara(e:Event) {
			if (Respuesta==resSel) {
				Padre.Wsiguiente.visible=true;
				var sonpop:PopUpSonido=new PopUpSonido();
			} else {
				Padre.cayuda.texto.text="¡Sigue intentando! Investiga, analiza y replantea tu respuesta. Con perseverancia nada es imposible.";
				var sonerr:ErrorSonido=new ErrorSonido();
			}
		}
	}
}