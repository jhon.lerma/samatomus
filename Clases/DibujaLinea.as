﻿package Clases{
	//importaciones de clases
	import Clases.ErrorSonido;
	import Clases.PopUpSonido;
	import flash.display.*;
	import flash.display.MovieClip;
	import flash.events.*;
	public class DibujaLinea {
		//declaracion de variables
		var Padre:Object=new Object();
		var dibuja:Boolean=true;
		var ejex:int;
		var ejey:int;
		var linea:Shape;
		var arrlinea:Array=new Array();
		var rLin:Object=new Object();
		var grup:Array=new Array();
		var cuadro1:String;
		var cuadro2:String;
		var respuestas:Array=new Array();
		var pasa:int;
		//declaracion de funciones
		public function DibujaLinea(padre) {
			Padre=padre;
			//Declaracion de eventos asi cada cuadro se puede relacionar con otros de esta lista de eventos
			padre.rojo.addEventListener(MouseEvent.CLICK,itmClick);
			padre.gris.addEventListener(MouseEvent.CLICK,itmClick);
			padre.azul.addEventListener(MouseEvent.CLICK,itmClick);
			padre.lila.addEventListener(MouseEvent.CLICK,itmClick);
			padre.citrato.addEventListener(MouseEvent.CLICK,itmClick);
			padre.oxalato.addEventListener(MouseEvent.CLICK,itmClick);
			padre.silicona.addEventListener(MouseEvent.CLICK,itmClick);
			padre.edta.addEventListener(MouseEvent.CLICK,itmClick);
			padre.breset.addEventListener(MouseEvent.CLICK,bresetClick);
			//declaracion de evento comparacion
			Padre.cayuda.bListo.addEventListener(MouseEvent.CLICK,compara);
		}
		function itmClick(e:MouseEvent) {
			e.target.removeEventListener(MouseEvent.CLICK,itmClick);
			if (dibuja==true) {
				ejex=Padre.mouseX;
				ejey=Padre.mouseY;
				cuadro1=e.target.name;
				linea=new Shape  ;

				Padre.addChild(linea);
				Padre.stage.addEventListener(MouseEvent.MOUSE_MOVE,MouseMove);
				dibuja=false;
			} else {
				Padre.stage.removeEventListener(MouseEvent.MOUSE_MOVE,MouseMove);
				cuadro2=e.target.name;

				rLin=new Object();
				rLin.cuadro1=cuadro1;
				rLin.cuadro2=cuadro2;
				arrlinea.push(linea);
				grup.push(rLin);//aqui hay algo raro

				cuadro1=null;
				cuadro2=null;
				//separa las listas
				trace("---------------------------------------");
				//se muestran en pantalla toda la lista de los objetos relacionados
				for each (var obj:* in grup) {
					trace(obj.cuadro1+" relacionado con "+obj.cuadro2);
					dibuja=true;
				}
			}
		}
		function bresetClick(e:Event) {
			for each (var obj:* in arrlinea) {
				Padre.removeChild(obj);
			}
			arrlinea=new Array();
			grup=new Array();
			Padre.rojo.addEventListener(MouseEvent.CLICK,itmClick);
			Padre.gris.addEventListener(MouseEvent.CLICK,itmClick);
			Padre.azul.addEventListener(MouseEvent.CLICK,itmClick);
			Padre.lila.addEventListener(MouseEvent.CLICK,itmClick);
			Padre.citrato.addEventListener(MouseEvent.CLICK,itmClick);
			Padre.oxalato.addEventListener(MouseEvent.CLICK,itmClick);
			Padre.silicona.addEventListener(MouseEvent.CLICK,itmClick);
			Padre.edta.addEventListener(MouseEvent.CLICK,itmClick);
			Padre.breset.addEventListener(MouseEvent.CLICK,bresetClick);
		}
		function MouseMove(e:MouseEvent):void {
			//limpia la pantalla para que no se vean multiples lineas durante el movimiento
			linea.graphics.clear();
			//define el estilo de linea este caso grosor 3 color rojo
			linea.graphics.lineStyle(4,0xff3355);
			//se le asignan las coordenadas de inicio a la linea
			linea.graphics.moveTo(ejex,ejey);
			//se le asignan las coordenadas de fin a la linea
			if (Padre.mouseY<=Padre.height*0.6) {
				linea.graphics.lineTo(Padre.mouseX+5,Padre.mouseY+5);
			} else if (Padre.mouseY>=Padre.height*0.6) {
				linea.graphics.lineTo(Padre.mouseX-5,Padre.mouseY-5);
			}
		}
		function compara(e:Event) {
			var itemR:Object=new Object  ;
			var itemC:Object=new Object  ;
			//se establece la relacion correcta entre dos cuadros y lo almacena en una lista de respuesta
			itemR=new Object  ;
			itemR.cuadro1="rojo";
			itemR.cuadro2="silicona";
			respuestas[0]=itemR;
			//se establece la relacion correcta entre dos cuadros y lo almacena en una lista de respuesta
			itemR=new Object  ;
			itemR.cuadro1="gris";
			itemR.cuadro2="oxalato";
			respuestas[1]=itemR;
			//se establece la relacion correcta entre dos cuadros y lo almacena en una lista de respuesta
			itemR=new Object  ;
			itemR.cuadro1="azul";
			itemR.cuadro2="citrato";
			respuestas[2]=itemR;
			//se establece la relacion correcta entre dos cuadros y lo almacena en una lista de respuesta
			itemR=new Object  ;
			itemR.cuadro1="lila";
			itemR.cuadro2="edta";
			respuestas[0]=itemR;
			//fin de respuestas
			for each (itemC in grup) {
				for each (itemR in respuestas) {
					if (itemR.cuadro1==itemC.cuadro1&&itemR.cuadro2==itemC.cuadro2||itemR.cuadro1==itemC.cuadro2&&itemR.cuadro2==itemC.cuadro1) {
						pasa++;
					}
				}
			}
			if (pasa==respuestas.length) {
				Padre.cayuda.texto.text="";
				//se muestra una ventana con tips sobre los tubos, sus componentes o por que son usados esa es la idea
				Padre.Wsiguiente.visible=true;
				var sonpop:PopUpSonido=new PopUpSonido();
				//se eliminan todas las lineas y todas las variables del modulo
				bresetClick(null);
			} else {
				bresetClick(null);
				Padre.cayuda.texto.text="¡Sigue intentando! Investiga, analiza y replantea tu respuesta. Con perseverancia nada es imposible.";
				pasa=0;
				var sonerr:ErrorSonido=new ErrorSonido();
			}
			pasa=0;
		}
	}
}