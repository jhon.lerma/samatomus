﻿package Clases{
	public class Pregunta{
		import flash.text.TextFormat
		public function Pregunta(texto:String,tamaño,padre){
			var formato:TextFormat=new TextFormat();
			formato.size=tamaño
			padre.areaP.texto.embedFonts=true
			padre.areaP.texto.text=texto
			padre.areaP.texto.setTextFormat(formato)
			padre.cayuda.texto.embedFonts=true
		}
	}
}