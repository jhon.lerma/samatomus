﻿package Clases{
	//importaciones de clases
	import Clases.ErrorSonido;
	import Clases.PopUpSonido;
	import flash.display.*;
	import flash.display.MovieClip;
	import flash.events.*;
	public class DibujaLinea {
		//declaracion de variables
		var Padre:Object=new Object  ;
		//var grup:Array=new Array();
		var ejex:int;
		var ejey:int;
		var linea:Shape;
		var rLin:Object=new Object  ;
		var grup:Array=new Array  ;
		var lineaStr:String;
		var cuadro1:String;
		var cuadro2:String;
		var respuestas:Array=new Array  ;
		var pasa:int;
		//declaracion de funciones
		public function DibujaLinea(padre) {
			Padre=padre;
			//Declaracion de eventos asi cada cuadro se puede relacionar con otros de esta lista de eventos
			padre.rojo.addEventListener(MouseEvent.MOUSE_DOWN,MouseDown);
			padre.rojo.addEventListener(MouseEvent.MOUSE_UP,MouseUp);
			padre.gris.addEventListener(MouseEvent.MOUSE_DOWN,MouseDown);
			padre.gris.addEventListener(MouseEvent.MOUSE_UP,MouseUp);
			padre.azul.addEventListener(MouseEvent.MOUSE_DOWN,MouseDown);
			padre.azul.addEventListener(MouseEvent.MOUSE_UP,MouseUp);
			padre.lila.addEventListener(MouseEvent.MOUSE_DOWN,MouseDown);
			padre.lila.addEventListener(MouseEvent.MOUSE_UP,MouseUp);
			padre.citrato.addEventListener(MouseEvent.MOUSE_DOWN,MouseDown);
			padre.citrato.addEventListener(MouseEvent.MOUSE_UP,MouseUp);
			padre.oxalato.addEventListener(MouseEvent.MOUSE_DOWN,MouseDown);
			padre.oxalato.addEventListener(MouseEvent.MOUSE_UP,MouseUp);
			padre.silicona.addEventListener(MouseEvent.MOUSE_DOWN,MouseDown);
			padre.silicona.addEventListener(MouseEvent.MOUSE_UP,MouseUp);
			padre.edta.addEventListener(MouseEvent.MOUSE_DOWN,MouseDown);
			padre.edta.addEventListener(MouseEvent.MOUSE_UP,MouseUp);
			//declaracion de evento comparacion
			Padre.cayuda.bListo.addEventListener(MouseEvent.CLICK,compara);
		}
		function MouseDown(event:MouseEvent):void {
			//brinda las coordenadas de inicio de la linea
			ejex=Padre.mouseX;
			ejey=Padre.mouseY;
			Padre.stage.addEventListener(MouseEvent.MOUSE_MOVE,MouseMove);
			//asigna el valor del cuadro de inicio
			cuadro1=event.target.name;
			//se crea un objeto de tipo linea
			linea=new Shape  ;
			//se asigna el nombre del objeto a una variable del modulo
			lineaStr=linea.name;
			//agrega una instancia del objeto linea a la pantalla
			Padre.addChild(linea);
		}
		function MouseMove(event:MouseEvent):void {
			//limpia la pantalla para que no se vean multiples lineas durante el movimiento
			linea.graphics.clear();
			//define el estilo de linea este caso grosor 3 color rojo
			linea.graphics.lineStyle(3,0xff3355);
			//se le asignan las coordenadas de inicio a la linea
			linea.graphics.moveTo(ejex,ejey);
			//se le asignan las coordenadas de fin a la linea
			if (Padre.mouseY<=Padre.height*0.6) {
				linea.graphics.lineTo(Padre.mouseX+5,Padre.mouseY+5);
			} else if (Padre.mouseY>=Padre.height*0.6) {
				linea.graphics.lineTo(Padre.mouseX-5,Padre.mouseY-5);
			}
			//Agrega un evento a la pantalla para saber si esta soltando la linea fuera de los cuadros
			Padre.stage.addEventListener(MouseEvent.MOUSE_UP,MouseUpStage);
		}
		function MouseUp(event:MouseEvent):void {
			//remueve el evento de la pantalla para evitar que cambien las coordenadas
			Padre.stage.removeEventListener(MouseEvent.MOUSE_MOVE,MouseMove);
			//asigna el nombre de la linea a una variable del modulo
			lineaStr=linea.name;
			//asigna el nombre del cuadro final a una variable del modulo
			cuadro2=event.target.name;
			//inicia la comparacion de relacion
			if (cuadro1==cuadro2) {
				try {
					Padre.removeChild(Padre.getChildByName(lineaStr));
				} catch (err:Error) {
					//do nothing
				}
			} else {
				//se crean el bojeto que contiene las propiedades de la union entre cuadros y linea
				rLin=new Object  ;
				//se le dan las propiedades
				rLin.linea=lineaStr;
				rLin.cuadro1=cuadro1;
				rLin.cuadro2=cuadro2;
				//se busca uno por uno en la lista para ver si existen objetos repetidos
				for each (var obj:Object=new Object   in grup) {
					if (obj.cuadro1==rLin.cuadro1||obj.cuadro1==rLin.cuadro2) {
						//elimina objetos repetidos
						Padre.removeChild(Padre.getChildByName(obj.linea));
						delete grup[grup.indexOf(obj)];
					} else {
						//do nothing
					}
				}
				//se busca uno por uno en la lista para ver si existen objetos repetidos
				for each (obj in grup) {
					if (obj.cuadro2==rLin.cuadro1||obj.cuadro2==rLin.cuadro2) {
						//elimina objetos repetidos
						Padre.removeChild(Padre.getChildByName(obj.linea));
						delete grup[grup.indexOf(obj)];
					} else {
						//do nothing
					}
				}
				grup[grup.length]=rLin;//aqui hay algo raro
				cuadro1=null;
				cuadro2=null;
				//separa las listas
				trace("---------------------------------------");
				//se muestran en pantalla toda la lista de los objetos relacionados
				for each (obj in grup) {
					trace(obj.cuadro1+" relacionado con "+obj.cuadro2+" por la linea "+obj.linea);
				}
			}
		}
		// elimina la linea si no esta sobe otro cuadrado
		function MouseUpStage(event:MouseEvent):void {

			if (cuadro2==null&&cuadro1!=null) {
				Padre.removeEventListener(MouseEvent.MOUSE_UP,MouseUpStage);
				Padre.removeEventListener(MouseEvent.MOUSE_MOVE,MouseMove);
				Padre.removeChild(linea);
			} else {
				Padre.removeEventListener(MouseEvent.MOUSE_MOVE,MouseMove);
			}
		}
		function compara(e:Event) {
			var itemR:Object=new Object  ;
			var itemC:Object=new Object  ;
			//se establece la relacion correcta entre dos cuadros y lo almacena en una lista de respuesta
			itemR=new Object  ;
			itemR.cuadro1="rojo";
			itemR.cuadro2="silicona";
			respuestas[0]=itemR;
			//se establece la relacion correcta entre dos cuadros y lo almacena en una lista de respuesta
			itemR=new Object  ;
			itemR.cuadro1="gris";
			itemR.cuadro2="oxalato";
			respuestas[1]=itemR;
			//se establece la relacion correcta entre dos cuadros y lo almacena en una lista de respuesta
			itemR=new Object  ;
			itemR.cuadro1="azul";
			itemR.cuadro2="citrato";
			respuestas[2]=itemR;
			//se establece la relacion correcta entre dos cuadros y lo almacena en una lista de respuesta
			itemR=new Object  ;
			itemR.cuadro1="lila";
			itemR.cuadro2="edta";
			respuestas[0]=itemR;
			//fin de respuestas
			for each (itemC in grup) {
				for each (itemR in respuestas) {
					if (itemR.cuadro1==itemC.cuadro1&&itemR.cuadro2==itemC.cuadro2||itemR.cuadro1==itemC.cuadro2&&itemR.cuadro2==itemC.cuadro1) {
						pasa++;
					}
				}
			}
			if (pasa==respuestas.length) {
				Padre.cayuda.texto.text="";
				//se muestra una ventana con tips sobre los tubos, sus componentes o por que son usados esa es la idea
				Padre.Wsiguiente.visible=true;
				var sonpop:PopUpSonido=new PopUpSonido();
				//se eliminan todas las lineas y todas las variables del modulo
				for each (itemC in grup) {
					trace(Padre.getChildByName(itemC.linea).name);
					Padre.removeChild(Padre.getChildByName(itemC.linea));
				}
				grup.splice(0,grup.length);
			} else {
				for each (itemC in grup) {
					trace(Padre.getChildByName(itemC.linea).name);
					Padre.removeChild(Padre.getChildByName(itemC.linea));
				}
				Padre.cayuda.texto.text="¡Sigue intentando! Investiga, analiza y replantea tu respuesta. Con perseverancia nada es imposible.";
				pasa=0;
				var sonerr:ErrorSonido=new ErrorSonido();
			}
			pasa=0;
			grup=new Array  ;
		}
	}
}