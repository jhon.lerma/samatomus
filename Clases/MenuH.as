﻿package Clases
{
	public class MenuH
	{
		import Clases.Pregunta;
		import Clases.ErrorSonido;
		import Clases.PopUpSonido;
		import fl.transitions.*;
		import fl.transitions.easing.*;
		import flash.display.*;
		import flash.xml.*;
		import flash.events.*;
		import flash.net.*;
		import fl.controls.ScrollBarDirection;
		import fl.data.DataProvider;
		import fl.controls.TileList;
		import fl.controls.listClasses.*;
		import fl.events.ListEvent;
		//declaracion de vaiables
		var Padre:Object = new Object  ;
		var Bloque:int;
		var Archivo:String;
		var Tamaño:int;
		var cargadorResp:URLLoader = new URLLoader  ;
		var cargadorList:URLLoader = new URLLoader  ;
		var cargadorArch:Loader = new Loader  ;
		var grup:MovieClip = new MovieClip  ;
		var ancho:int;
		var alto:int;
		var fin:int = 1;
		var duracion:Number = 0.5;
		var contador:int;
		var escala:Number = 0.65;
		var listaResp:Array = new Array  ;
		var listaArch:Array = new Array  ;
		var listaNomb:Array = new Array  ;
		var objBandeja:Array = new Array  ;
		var cuadricula:TileList = new TileList  ;
		var dp:DataProvider = new DataProvider  ;
		//declaracion de funciones
		public function MenuH(archivo:String,bloque:int,tamaño:int,padre)
		{
			//asigna padre
			Padre = padre;
			Bloque = bloque;
			Archivo = archivo;
			Tamaño = tamaño;
			//crea mascara
			padre.menudes.addChild(grup);
			grup.mask = padre.menudes.mascara;
			grup.x = padre.menudes.grupo.x;
			grup.y = padre.menudes.grupo.y;
			padre.menudes.grupo.visible = false;
			//crea cuadricula
			Padre.bandeja.addChild(cuadricula);
			cuadricula.name = "lista";
			cuadricula.dataProvider = dp;
			cuadricula.setSize(Padre.bandeja.width - 9,Padre.bandeja.height - 9);
			cuadricula.direction = ScrollBarDirection.VERTICAL;
			cuadricula.x = 2;
			cuadricula.y = 2;
			//carga el archivo xml
			cargadorResp.load(new URLRequest("XML/" + Archivo));
			cargadorResp.addEventListener(Event.COMPLETE,respuestas);
			cargadorList.load(new URLRequest("XML/lista_insumos.xml"));
			cargadorList.addEventListener(Event.COMPLETE,crear);
			//agrega eventos a los botones izq der;
			Padre.menudes.Izq.addEventListener(MouseEvent.MOUSE_DOWN,clkIzq);
			Padre.menudes.Der.addEventListener(MouseEvent.MOUSE_DOWN,clkDer);
		}
		function crear(e:Event)
		{
			var XmlExterno:XML = new XML(cargadorList.data);
			//recorre el xml y agrega los elementos
			for each (var nodo:XML in XmlExterno.elements())
			{
				listaArch[listaArch.length] = nodo. @ archivo.toString();
				listaNomb[listaNomb.length] = nodo. @ nombre.toString();
			}
			cargar();
		}
		function cargar()
		{
			cargadorArch.load(new URLRequest(listaArch[contador]));
			cargadorArch.contentLoaderInfo.addEventListener(Event.COMPLETE,cargado);
			contador++;
		}
		function cargado(e:Event)
		{
			var mClip:MovieClip = new MovieClip  ;
			var bMap:Bitmap = new Bitmap  ;
			bMap = Bitmap(e.target.content);
			bMap.x =  -  bMap.width * 0.5;
			bMap.y =  -  bMap.height * 0.5;
			bMap.smoothing = true;
			var bg_width = bMap.width + 10;
			var bg_height = bMap.height + 10;
			ancho = bMap.width;
			alto = bMap.height;
			mClip.addChild(bMap);
			mClip.graphics.lineStyle(4,0x000000);
			mClip.graphics.beginFill(0x999999);
			mClip.graphics.drawRoundRect( -  bg_width * 0.5, -  bg_height * 0.5,bg_width,bg_height,20);
			mClip.graphics.endFill();
			mClip.name = listaNomb[contador - 1];
			mClip.buttonMode = true;
			mClip.addEventListener(MouseEvent.MOUSE_OVER,itm_sobre);
			mClip.addEventListener(MouseEvent.MOUSE_OUT,itm_fuera);
			mClip.addEventListener(MouseEvent.CLICK,itm_click);
			mClip.scaleX = mClip.scaleY = escala;
			mClip.x = grup.numChildren * 120;
			mClip.y = Padre.menudes.grupo.y;
			grup.addChildAt(mClip,0);
			if (contador < listaArch.length)
			{
				cargar();
			}
		}
		function itm_sobre(e:Event)
		{
			var mc:MovieClip = MovieClip(e.target);
			grup.addChild(mc);
			var altoTween:Tween = new Tween(e.target,"scaleX",Elastic.easeOut,escala,fin,duracion,true);
			var anchoTween:Tween = new Tween(e.target,"scaleY",Elastic.easeOut,escala,fin,duracion,true);
		}
		function itm_fuera(e:Event)
		{
			var altoTween:Tween = new Tween(e.target,"scaleX",Elastic.easeOut,e.target.scaleX,escala,duracion,true);
			var anchoTween:Tween = new Tween(e.target,"scaleY",Elastic.easeOut,e.target.scaleX,escala,duracion,true);
		}
		function itm_click(e:Event)
		{
			var bMap:Bitmap = e.target.getChildAt(0);
			cuadricula.columnWidth = bMap.width / 1.5;
			cuadricula.rowHeight = bMap.height / 1.5;
			cuadricula.addItem({name:e.target.name,source:bMap});
			cuadricula.verticalScrollPosition = cuadricula.maxVerticalScrollPosition;
			e.target.removeEventListener(MouseEvent.CLICK,itm_click);
			cuadricula.addEventListener(ListEvent.ITEM_CLICK,citm_click);
		}
		function citm_click(e:ListEvent)
		{
			var bMap:Bitmap = new Bitmap  ;
			bMap = Bitmap(e.item.source);
			Padre.menudes.getChildByName(grup.name).getChildByName(e.item.name).addChild(bMap);
			bMap.x =  -  bMap.width * 0.5;
			bMap.y =  -  bMap.height * 0.5;
			Padre.menudes.getChildByName(grup.name).getChildByName(e.item.name).addEventListener(MouseEvent.CLICK,itm_click);
			cuadricula.removeItem(e.item);
		}
		function clkIzq(e:Event)
		{
			var xTween:Tween;
			xTween = new Tween(grup,"x",Back.easeOut,grup.x,grup.x + 312,duracion,true);
			xTween.addEventListener(TweenEvent.MOTION_CHANGE,controlIzq);
		}
		function clkDer(e:Event)
		{
			var xTween:Tween;
			xTween = new Tween(grup,"x",Back.easeOut,grup.x,grup.x - 312,duracion,true);
			xTween.addEventListener(TweenEvent.MOTION_CHANGE,controlDer);
		}
		function controlDer(e:Event)
		{
			if (grup.x + grup.width <= Padre.menudes.grupo.x + Padre.menudes.grupo.width)
			{
				e.target.stop();
				grup.x =  -  grup.width + Padre.menudes.grupo.x + Padre.menudes.grupo.width;
			}
		}
		function controlIzq(e:Event)
		{
			if (grup.x >= Padre.menudes.grupo.x)
			{
				e.target.stop();
				grup.x = Padre.menudes.grupo.x;
			}
		}
		//------------------------------------------------------------
		//************************************************************
		function respuestas(e:Event)
		{
			var Xml:XML = new XML(cargadorResp.data);
			//recorre el xml y agrega los elementos
			for each (var nodXml:XML in Xml.respuesta)
			{
				//se crean los objetos a ordenar
				if (nodXml. @ indice == Bloque)
				{
					var preg:Pregunta = new Pregunta(nodXml. @ pregunta,Tamaño,Padre);
					for each (var childNodXml:XML in nodXml.insumo)
					{
						listaResp[listaResp.length] = childNodXml.toString();
					}
				}
			}
			Padre.cayuda.bListo.addEventListener(MouseEvent.CLICK,compara);
		}
		function compara(e:Event)
		{
			var pasa:int;
			var res:String;
			if (dp.length == listaResp.length)
			{
				for (var num:int; num < dp.length; num++)
				{
					for each (res in listaResp)
					{
						if (res == dp.getItemAt(num).name)
						{
							pasa++;
						}
					}
				}
			}
			if (pasa == listaResp.length)
			{
				Padre.cayuda.texto.text = "";
				//colocar llamada a ventana
				Padre.Wsiguiente.visible = true;
				var sonpop:PopUpSonido=new PopUpSonido();
			}
			else
			{
				Padre.cayuda.texto.text = "¡Sigue intentando! Investiga, analiza y replantea tu respuesta. Con perseverancia nada es imposible.";
				var sonerr:ErrorSonido = new ErrorSonido  ;
				pasa = 0;
			}
		}
	}
}