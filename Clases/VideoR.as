﻿package Clases{
	import Clases.PopUpSonido;
	import fl.video.FLVPlayback;
	import fl.video.VideoEvent;
	import flash.events.MouseEvent;
	public class VideoR {
		var Padre:Object=new Object  ;
		public function VideoR(archivo:String,padre) {
			Padre=padre;
			padre.reproductor.volver.visible=false;
			padre.reproductor.pantalla.source="Videos/"+archivo;
			padre.reproductor.pantalla.addEventListener(VideoEvent.COMPLETE,mostrar);
			padre.reproductor.pantalla.addEventListener(VideoEvent.PLAYING_STATE_ENTERED,ocultar);
			padre.reproductor.volver.addEventListener(MouseEvent.CLICK,devMenu);
		}
		function devMenu(e:MouseEvent) {
			var popson:PopUpSonido=new PopUpSonido();
			Padre.gotoAndPlay(2);
		}
		function mostrar(e:VideoEvent) {
			Padre.reproductor.volver.visible=true;
			Padre.reproductor.pantalla.stop();
		}
		function ocultar(e:VideoEvent) {
			Padre.reproductor.volver.visible=false;
		}
	}
}