﻿package Clases{
	public class Ordenar {
		import Clases.ErrorSonido;
		import Clases.PopUpSonido;
		import flash.xml.*;
		import flash.text.TextFormat;
		import flash.text.TextFormatAlign;
		import flash.text.TextField;
		import flash.display.*;
		import flash.events.*;
		import flash.net.*;
		import fl.controls.ScrollBarDirection;
		import fl.controls.TileList;
		import fl.data.DataProvider;
		import fl.controls.listClasses.*;
		import fl.events.ListEvent;
		//declaracion de variables
		var cargador:URLLoader=new URLLoader();
		var listaXml:Array=new Array();
		var Padre:Object=new Object();
		var Numero:Object=new Object();
		var target:Sprite;
		var tarPosX:int;
		var tarPosY:int;
		var targetlist:TileList=new TileList();
		var draglist:TileList=new TileList();
		var datProTar:DataProvider=new DataProvider();
		var pasa:int;
		public function Ordenar(archivo:String,padre) {
			Padre=padre;
			cargador.load(new URLRequest("XML/"+ archivo));
			cargador.addEventListener(Event.COMPLETE, cargado);
			Padre.cayuda.bListo.addEventListener(MouseEvent.CLICK,compara);
			//se crea la lista target
			Padre.tabla.addChild(targetlist);
			//targetlist.name="lista";
			targetlist.dataProvider=datProTar;
			targetlist.setSize(Padre.tabla.width,Padre.tabla.height-9);
			targetlist.direction=ScrollBarDirection.VERTICAL;
			targetlist.x=2;
			targetlist.y=2;
			targetlist.columnWidth=Padre.tabla.width-20;
			targetlist.rowHeight=50;
			targetlist.addEventListener(ListEvent.ITEM_CLICK,tar_click);
			//se crea la lista drag
			Padre.desorden.addChild(draglist);
			//draglist.name="lista";
			draglist.setSize(Padre.desorden.width,Padre.desorden.height-9);
			draglist.direction=ScrollBarDirection.VERTICAL;
			draglist.x=2;
			draglist.y=2;
			draglist.columnWidth=Padre.desorden.width-20;
			draglist.rowHeight=50;
			draglist.addEventListener(ListEvent.ITEM_CLICK,drag_click);
		}
		function cargado(e:Event) {
			//se crea el formato del texto
			var formato:TextFormat=new TextFormat();
			formato.size=22;
			formato.font="Opal";
			formato.align=TextFormatAlign.CENTER;
			var Xml:XML=new XML(cargador.data);
			//recorre el xml y agrega los elementos
			for each (var nodXml:XML in Xml.elements()) {
				//se crean los objetos a ordenar
				listaXml[listaXml.length]=nodXml.@objeto.toString();
				target=new Sprite();
				//se crea el texto contenido
				var texto:TextField=new TextField();
				texto.embedFonts=true;
				texto.autoSize="center";
				texto.text=nodXml.@objeto.toString();
				texto.x=5;
				texto.y=5;
				texto.multiline=true;
				texto.wordWrap=true;
				texto.width=draglist.width;
				texto.selectable=false;
				texto.mouseEnabled=false;
				texto.setTextFormat(formato);
				target.addChildAt(texto,0);
				//se dibuja el cuadro
				target.name=nodXml.@nombre;
				//trace(target.name)
				target.graphics.lineStyle(4,0x000000);
				target.graphics.beginFill(0x9988ee,0.9);
				target.graphics.drawRoundRect(0,0,texto.width+10,texto.height+10,10,10);
				draglist.addItem({label:null, source:target});
			}
		}
		function drag_click(e:ListEvent) {
			draglist.removeItem(e.item);
			targetlist.addItem(e.item);
			targetlist.verticalScrollPosition=targetlist.maxVerticalScrollPosition;
		}
		function tar_click(e:ListEvent) {
			targetlist.removeItem(e.item);
			draglist.addItem(e.item);
			draglist.verticalScrollPosition=draglist.maxVerticalScrollPosition;
		}
		function compara(e:Event) {
			trace("Listaxml: "+listaXml.length+" datProTar: "+datProTar.length);
			if (listaXml.length==datProTar.length) {
				for(var num:int;num<datProTar.length;num++){;
				if (datProTar.getItemAt(num).source.name==1+datProTar.getItemIndex(datProTar.getItemAt(num))) {
					pasa++;
				}
			}
		}
		if (pasa==listaXml.length) {
			Padre.Wsiguiente.visible=true;
			Padre.cayuda.texto.text="";
			var sonpop:PopUpSonido=new PopUpSonido();
		} else {
			pasa=0;
			Padre.cayuda.texto.text="¡Sigue intentando! Investiga, analiza y replantea tu respuesta. Con perseverancia nada es imposible.";
			var sonerr:ErrorSonido=new ErrorSonido();
		}
	}
}
}