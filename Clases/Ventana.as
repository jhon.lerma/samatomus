﻿package Clases{
	import flash.display.*;
	import flash.net.URLRequest;
	import flash.events.*;
	import flash.text.*;
	import flash.geom.Rectangle;
	public class Ventana {
		public var Wpadre:Object=new Object();
		public function Ventana(X:int,Y:int,Titulo:String,Texto:String,Padre) {
			//declaracion de variable
			Padre.Wsiguiente.visible=false
			var ftTexto:TextFormat=new TextFormat();
			var ftTitulo:TextFormat=new TextFormat();
			//se asigna posicion inicial
			Wpadre=Padre;
			Padre.Wsiguiente.x=X;
			Padre.Wsiguiente.y=Y;
			//se asignan proporciones 
			Padre.Wsiguiente.cuerpo.x=Padre.Wsiguiente.barratitulo.x;
			Padre.Wsiguiente.cuerpo.y=Padre.Wsiguiente.barratitulo.y+Padre.Wsiguiente.barratitulo.height+3;
			Padre.Wsiguiente.cuerpo.width=Padre.Wsiguiente.barratitulo.width;
			//se crea el formato del texto del cuerpo
			ftTexto.size=20;
			ftTexto.font="Arial";
			ftTexto.align="justify";
			ftTexto.leftMargin=15;
			ftTexto.rightMargin=15;
			ftTexto.leading=5;
			//se asigna el formato
			Padre.Wsiguiente.cuerpo.texto.editable=false
			Padre.Wsiguiente.cuerpo.texto.setStyle("textFormat",ftTexto);
			//se asigna el texto
			Padre.Wsiguiente.cuerpo.texto.htmlText=Texto;
			//se asigna el formato del titulo
			ftTitulo.size=20;
			ftTitulo.align="center";
			Padre.Wsiguiente.barratitulo.titulo.setTextFormat(ftTitulo);
			//se asigna el titulo
			Padre.Wsiguiente.barratitulo.titulo.text=Titulo;
			//se asignan eventos
			Padre.Wsiguiente.barratitulo.addEventListener(MouseEvent.MOUSE_DOWN,barraDown);
			Padre.Wsiguiente.barratitulo.addEventListener(MouseEvent.MOUSE_UP,barraUp);
			Padre.Wsiguiente.barratitulo.cerrar.addEventListener(MouseEvent.CLICK,cerrarV)
			Padre.Wsiguiente.cuerpo.siguiente.addEventListener(MouseEvent.CLICK,cerrarV)
		}
		function barraDown(e:Event) {
			Wpadre.Wsiguiente.startDrag();
		}
		function barraUp(e:Event) {
			Wpadre.Wsiguiente.stopDrag();
		}
		function cerrarV(e:Event) {
			Wpadre.Wsiguiente.parent.play();
		}
	}
}