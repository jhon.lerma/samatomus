﻿package Clases{
	public class ErrorSonido {
		import flash.media.*;
		import flash.net.URLRequest;
		import flash.events.*;
		var cargaSonido:URLRequest=new URLRequest("Sonidos/error.mp3");
		var sonido:Sound=new Sound(cargaSonido);
		public function ErrorSonido() {
			sonido.play();
		}
	}
}