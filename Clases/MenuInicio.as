﻿package Clases{
	public class MenuInicio {
		import flash.display.*;
		import flash.events.*;
		var Padre:Object=new Object();
		public function MenuInicio(padre) {
			Padre=padre;
			Padre.Vcuidados.addEventListener(MouseEvent.CLICK,vcuiclk);
			Padre.Vvenosa.addEventListener(MouseEvent.CLICK,vvenclk);
			Padre.Varterial.addEventListener(MouseEvent.CLICK,vartclk);
			Padre.Vcapilar.addEventListener(MouseEvent.CLICK,vcapclk);
			Padre.Svenosa.addEventListener(MouseEvent.CLICK,svenclk);
			Padre.Sarterial.addEventListener(MouseEvent.CLICK,sartclk);
			Padre.Scapilar.addEventListener(MouseEvent.CLICK,scapclk);
			Padre.Creditos.addEventListener(MouseEvent.CLICK,credits);
		}
		function vcuiclk(e:Event) {
			Padre.gotoAndPlay(3);
		}
		function vvenclk(e:Event) {
			Padre.gotoAndPlay(4);
		}
		function vartclk(e:Event) {
			Padre.gotoAndPlay(5);
		}
		function vcapclk(e:Event) {
			Padre.gotoAndPlay(6);
		}
		function vconclk(e:Event) {
			Padre.gotoAndPlay(7);
		}
		function svenclk(e:Event) {
			Padre.gotoAndPlay(8);
		}
		function sartclk(e:Event) {
			Padre.gotoAndPlay(30);
		}
		function scapclk(e:Event) {
			Padre.gotoAndPlay(45);
		}
		function credits(e:Event) {
			Padre.gotoAndPlay(53);
		}
	}
}