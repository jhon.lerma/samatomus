﻿package Clases{
	public class Arrastrar {
		import Clases.Pregunta;
		import Clases.ErrorSonido;
		import Clases.PopUpSonido;
		import flash.filters.*;
		import flash.events.*;
		import flash.display.*;
		import flash.net.*;
		import flash.xml.*;
		import flash.text.*;
		var Padre:Object=new Object();
		var Tamaño:int;
		var resSel:String;
		var cargador:URLLoader=new URLLoader();
		var listaXml:Array=new Array();
		var Bloque:int;
		var posY:int;
		var Respuesta:String;

		public function Arrastrar(archivo:String,bloque:int,tamaño:int,padre) {
			Padre=padre;
			Bloque=bloque;
			Tamaño=tamaño;
			Padre.cayuda.bListo.addEventListener(MouseEvent.CLICK,compara);
			cargador.dataFormat=URLLoaderDataFormat.TEXT;
			cargador.load(new URLRequest("XML/"+archivo));
			cargador.addEventListener(Event.COMPLETE,cargado);
		}
		function cargado(e:Event) {
			var Xml:XML=new XML(e.target.data);
			for each (var nodXml:XML in Xml.pregunta) {
				if (nodXml.@indice==Bloque) {
					var preg:Pregunta=new Pregunta(nodXml.@pregunta,Tamaño,Padre);
					Respuesta=nodXml.respuesta;
					for each (var childNodXml:XML in nodXml.item) {
						var preItem:Sprite=new Sprite  ;
						var texto:TextField=new TextField();
						var formato:TextFormat=new TextFormat();
						formato.font="Opal";
						formato.align=TextFormatAlign.CENTER
						formato.size=25;
						texto.autoSize="left";
						texto.embedFonts=true;
						texto.text=childNodXml;
						texto.x=5;
						texto.y=5;
						texto.multiline=true;
						texto.wordWrap=true;
						texto.width=600
						texto.selectable=false;
						texto.mouseEnabled=false;
						texto.setTextFormat(formato);
						preItem.addChild(texto);
						//se dibuja el cuadro
						preItem.name=childNodXml;
						//trace(target.name)
						preItem.graphics.lineStyle(4,0x000000);
						preItem.graphics.beginFill(0x9988ee,0.9);
						preItem.graphics.drawRoundRect(0,0,texto.width+10,texto.height+10,10,10);
						preItem.addEventListener(MouseEvent.MOUSE_DOWN,arrastrar);
						preItem.addEventListener(MouseEvent.MOUSE_UP,soltar);
						Padre.contenedor.contenedor.addChild(preItem);
						preItem.y=posY;
						posY=posY+preItem.height+20;
					}
				}
			}
		}
		function arrastrar(e:Event) {
			Padre.contenedor.contenedor.addChildAt(e.target,Padre.contenedor.contenedor.numChildren);
			e.target.startDrag();
		}
		function soltar(e:Event) {
			e.target.stopDrag();
			if (Padre.contenedor.objetivo.hitTestObject(e.target)) {
				resSel=e.target.name;
				e.target.x=Padre.contenedor.objetivo.width*0.5-e.target.width*0.5;
				e.target.y=Padre.contenedor.objetivo.y+((Padre.contenedor.objetivo.height-e.target.height)*0.5);
			}
		}
		function compara(e:Event) {
			if (Respuesta==resSel) {
				Padre.Wsiguiente.visible=true;
				var sonpop:PopUpSonido=new PopUpSonido();
			} else {
				Padre.cayuda.texto.text="¡Sigue intentando! Investiga, analiza y replantea tu respuesta. Con perseverancia nada es imposible.";
				var sonerr:ErrorSonido=new ErrorSonido();
			}
		}
	}
}